const http = require('http');
const hostname = '0.0.0.0';

const port = process.env.PUERTO_SERVICIO;
const ip_base_datos = process.env.IP_BASE_DATOS;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.end(
        `<html>
            <body>
                <h1>Hola soy un servicio, y la IP de mi base de datos es: ${ip_base_datos}</h1>
            </body>
        </html>`
    );
});

server.listen(port, hostname);